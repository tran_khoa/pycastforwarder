# coding=utf-8
from winpcapy import WinPcapUtils, WinPcapDevices
import struct

from pypacker.layer12 import ethernet
from pypacker.layer3 import ip
from pypacker.layer4 import udp, ssl
from pypacker.layer567 import dns

import dpkt

vpn_device = None

class BroadcastForwarder:
    main_dev = None
    vpn_dev = None
    def __init__(self):
        self.init_devices()
        self.init_callback()

    def init_devices(self):

        dev_list = []

        with WinPcapDevices() as devices:
            for i, dev in enumerate(devices):
                dev_list.append(dev)

        print("Gebe die Nummer des Interfaces vom VPN ein.")
        for (i, dev) in enumerate(dev_list):
            print("[{:d}] {}".format(i, dev.description))

        vpn_choice = input("-> ")
        self.vpn_dev = dev_list[int(vpn_choice)] #@TODO unsicher
        global vpn_device
        vpn_device = self.vpn_dev
        dev_list.remove(self.vpn_dev)

        if len(dev_list) == 1:
            self.main_dev = dev_list[0]
        else:
            print("Gebe die Nummer des Interfaces vom Haupt-Interface ein.")
            for (i, dev) in enumerate(dev_list):
                print("[%d] %s" % (i, dev.description))
                self.main_dev = dev_list[int(input("-> "))]  # @TODO unsicher

    def init_callback(self):
        global vpn_device
        WinPcapUtils.capture_on(self.main_dev.description.decode('ascii'), on_packet_received)

#TODO adapter automatisch auswaehlen
#TODO
def on_packet_received(win_pcap, param, header, pkt_data):
    try:
        eth = ethernet.Ethernet(pkt_data)

        if eth[ip.IP] is None or eth[udp.UDP] is None or eth[ssl.SSL] is not None or eth[dns.DNS] is not None:
            return

        src_ip = eth[ip.IP].src_s
        dst_ip = eth[ip.IP].dst_s
        src_port = eth[udp.UDP].sport
        dst_port = eth[udp.UDP].dport

        if dst_ip != "255.255.255.255":
            return

        pkt = ethernet.Ethernet(dst_s=eth[ethernet.Ethernet].dst_s,src_s=eth[ethernet.Ethernet].src_s) + \
              ip.IP(src_s="17.17.17.17", dst_s=dst_ip, p=ip.IP_PROTO_UDP) + \
              udp.UDP(sport=src_port, dport=dst_port)
        pkt[udp.UDP].body_bytes = eth[udp.UDP].body_bytes

        print("Packet by pypacker:\n{0}", pkt.bin())

        global vpn_device
        print('TAP-Windows Adapter V9')
        WinPcapUtils.send_packet('TAP-Windows Adapter V9', pkt.bin())
        WinPcapUtils.send_packet("*Ethernet*", pkt.bin())
    except struct.error:
        pass


br = BroadcastForwarder()
