from winpcapy import WinPcapUtils, WinPcapDevices
import socket
import dpkt
import time

from pypacker.layer12 import ethernet
from pypacker.layer3 import ip
from pypacker.layer4 import udp
dev_list = []

with WinPcapDevices() as devices:
    for i, dev in enumerate(devices):
        dev_list.append(dev)

print("Gebe die Nummer des Interfaces vom VPN ein.")
for (i, dev) in enumerate(dev_list):
    print("[{:d}] {}".format(i, dev.description))

vpn_choice = input("-> ")
vpn_dev = dev_list[int(vpn_choice)]  # @TODO unsicher

# build udp packet
u = dpkt.udp.UDP(
    dport=21701,
    sport=21701,
    data=b"Hallo Welt"
)
u.ulen = len(u)

# build ip packet
i = dpkt.ip.IP(
    dst=socket.inet_aton("255.255.255.255"),
    src=socket.inet_aton("137.226.182.85"),
    data=u,
    p=dpkt.ip.IP_PROTO_UDP
)
i.len = len(i)

pkt = ip.IP(src_s="137.226.182.85", dst_s="255.255.255.255", p=ip.IP_PROTO_UDP) + \
                  udp.UDP(sport=21701, dport=21701)
pkt[udp.UDP].body_bytes = b"Hallo Welt"


print("Packet by pypacker:\n{0}", pkt.bin())
print("Packet by dpkt:\n{0}", i.pack())

dev_name = vpn_dev.description.decode("ascii")
print(dev_name)
while True:
    time.sleep(1)
    print(i.pack())
    WinPcapUtils.send_packet(dev_name, i.pack())
